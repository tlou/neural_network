//
// Created by tlou on 02.03.17.
//

#include <fstream>
#include <sstream>
#include "g2048.hpp"
#include "NeuralNetwork.hpp"

int main()
{
  loco::NeuralNetwork network;
  loco::G2048 game;
  network.setDimension(game.getFeatureDimension(), 4);
  network.generateLayers(2, 500, 1e-3f);
  std::stringstream ss;
  for(int iter = 0; iter < 1000; ++iter)
  {
    if(iter % 100 == 0)
    {
      std::cout << iter << std::endl;
    }
    game.reset();
    bool is_finished = false;
    while(!is_finished)
    {
      std::vector<int> possible_op = game.getPossibleOperation();
      if(!possible_op.empty())
      {
        int op = network.forwards(game.getFeature(), possible_op);
        is_finished = !game.proc(op);
        if(game.getGain() > 0.0f)
        {
          network.backwards(op);
        }
        else
        {
          network.backwards(-1, -game.getGain());
        }
//        network.backwards(op, game.getGain());
      }
      else
      {
        is_finished = true;
      }

      if(is_finished)
      {
        ss << game.getScore() << std::endl;
      }
    }
  }

  std::ofstream log;
  log.open("/home/tlou/log.txt");
  log << ss.str();
  log.close();
  return 0;
}