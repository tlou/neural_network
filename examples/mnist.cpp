#include <cstdlib>
#include <iostream>
#include <string>

#include "TrainerMnist.hpp"

int main(int argn, char **argv)
{
  std::string path_data("/home/tlou/workspace/mnist/");
  loco::TrainerMnist trainer(path_data + "train-images-idx3-ubyte",
                             path_data + "train-labels-idx1-ubyte",
                             path_data + "t10k-images-idx3-ubyte",
                             path_data + "t10k-labels-idx1-ubyte");
  trainer.setDimension(784, 10);
  trainer.generateLayers(2, 300, 1e-4);
  trainer.train(5);
  trainer.saveNetwork("/tmp/asd.nn");
  trainer.printTest();
  trainer.loadNetwork("/tmp/asd.nn");
  trainer.printTest();
  return 0;
}