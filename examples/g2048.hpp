#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <algorithm>

namespace loco
{
  class G2048
  {
  protected:
    const int _size;
    const int _total;
    const int _stage;
    const float _weight_space;
    const float _weight_score;
    std::vector<int> _board;
    int _score;
    float _gain;

    std::vector<int> pushUp(const std::vector<int> board)
    {
      std::vector<int> board_(board);
      for(int id = _size; id < _total; ++id)
      {
        if(board_.at(id) == 0)
        {
          continue;
        }

        int id_this = id, id_next = (id - _size);
        while(id_next >= 0)
        {
          if(board_.at((size_t)id_this) == board_.at((size_t)id_next))
          {
            ++board_.at((size_t)id_next);
            board_.at((size_t)id_this) = 0;
            _score += 1 << board_.at((size_t)id_next);
            break;
          }
          else if(board_.at((size_t)id_next) == 0)
          {
            std::swap(board_.at((size_t)id_this), board_.at((size_t)id_next));
          }
          else
          {
            break;
          }
          id_next -= _size;
          id_this -= _size;
        }
      }
      return board_;
    }

    std::vector<int> pushRight(const std::vector<int> board)
    {
      std::vector<int> board_(board);
      for(int id0 = _size - 2; id0 >= 0; --id0)
      {
        for(int id = id0; id < _total; id += _size)
        {
          if(board_.at((size_t)id) == 0)
          {
            continue;
          }

          int id_this = id, id_next = (id + 1);
          while(id_next % _size)
          {
            if(board_.at((size_t)id_this) == board_.at((size_t)id_next))
            {
              ++board_.at((size_t)id_next);
              board_.at((size_t)id_this) = 0;
              _score += 1 << board_.at((size_t)id_next);
              break;
            }
            else if(board_.at((size_t)id_next) == 0)
            {
              std::swap(board_.at((size_t)id_this), board_.at((size_t)id_next));
            }
            else
            {
              break;
            }
            ++id_next;
            ++id_this;
          }
        }
      }
      return board_;
    }

    std::vector<int> pushDown(const std::vector<int> board)
    {
      std::vector<int> board_(board);
      for(int id = _total - 1 - _size; id >= 0; --id)
      {
        if(board_.at((size_t)id) == 0)
        {
          continue;
        }

        int id_this = id, id_next = (id + _size);
        while(id_next < _total)
        {
          if(board_.at((size_t)id_this) == board_.at((size_t)id_next))
          {
            ++board_.at((size_t)id_next);
            board_.at((size_t)id_this) = 0;
            _score += 1 << board_.at((size_t)id_next);
            break;
          }
          else if(board_.at((size_t)id_next) == 0)
          {
            std::swap(board_.at((size_t)id_this), board_.at((size_t)id_next));
          }
          else
          {
            break;
          }
          id_next += _size;
          id_this += _size;
        }
      }
      return board_;
    }

    std::vector<int> pushLeft(const std::vector<int> board)
    {
      std::vector<int> board_(board);
      for(int id0 = 1; id0 < _size; ++id0)
      {
        for(int id = id0; id < _total; id += _size)
        {
          if(board_.at((size_t)id) == 0)
          {
            continue;
          }

          int id_this = id, id_next = id - 1;
          while(id_this % _size)
          {
            if(board_.at((size_t)id_this) == board_.at((size_t)id_next))
            {
              ++board_.at((size_t)id_next);
              board_.at((size_t)id_this) = 0;
              _score += 1 << board_.at((size_t)id_next);
              break;
            }
            else if(board_.at((size_t)id_next) == 0)
            {
              std::swap(board_.at((size_t)id_this), board_.at((size_t)id_next));
            }
            else
            {
              break;
            }
            --id_next;
            --id_this;
          }
        }
      }
      return board_;
    }

    std::vector<int> tryPush(const int direction)
    {
      switch((direction + 4) % 4)
      {
        case 0:
          return pushUp(_board);

        case 1:
          return pushRight(_board);

        case 2:
          return pushDown(_board);

        case 3:
          return pushLeft(_board);

        default:
          return _board;
      }
    }

    void push(const int direction)
    {
      _board = tryPush(direction);
    }

    bool add()
    {
      std::vector<int> zeroes = getZeroes();
      if(zeroes.empty())
      {
        return false;
      }
      else
      {
        _board.at((size_t)zeroes.at(rand() % zeroes.size())) = 1;
        return true;
      }
    }

    std::vector<int> getZeroes()
    {
      std::vector<int> zeroes;
      zeroes.reserve((size_t)_total);
      for(size_t i = 0; i < _total; ++i)
      {
        if(_board.at(i) == 0)
        {
          zeroes.push_back(i);
        }
      }
      return zeroes;
    }


  public:
    G2048() : _size(4), _total(_size * _size), _weight_space(1.0f), _weight_score(0.1f), _stage(20)
    {
      srand((unsigned int)time(NULL));
      reset();
    }

    ~G2048()
    {}

    void reset()
    {
      _board = std::vector<int>((size_t)_total, 0);
      _score = 0;
      _gain = 0.0f;
      add();
      add();
    }

    bool proc(const int direction)
    {
      int num_zero_last = (int)getZeroes().size();
      int score_last = _score;
      push(direction);
      _gain = _weight_space * (float) (((int) getZeroes().size()) - num_zero_last - 1)
              + _weight_score * (float)log((float) (_score - score_last) + 1.0f);
//      std::cout << "gain: " << _gain << std::endl;
      return add();
    }

    float getGain() const
    {
      return _gain;
    }

    void print() const
    {
      std::cout << "\n";
      for(size_t i = 0; i < _board.size(); ++i)
      {
        if(i % _size == 0)
        {
          std::cout << "\n";
        }
        std::cout << "|";
        if(_board.at(i) > 0)
        {
          std::cout << (1 << _board.at(i));
        }
        std::cout << "\t";
      }
      std::cout << "\n";
      std::cout << "score: " << _score << std::endl;
    }

    std::vector<float> getFeature() const
    {
      const static float weight = (float)sqrt((float) (_stage * _stage) / (float) (_stage * _stage - 2 * _stage + 2));
      std::vector<float> feature((size_t)_stage * _total, -weight / (float) _stage);
      const static float val_nonzero = (float) (_stage - 1) / (float) _stage;
      for(int i = 0; i < _total; ++i)
      {
        feature.at((size_t)i * _stage + _board.at(i)) = val_nonzero;
      }
      return feature;
    }

    int getScore() const
    {
      return _score;
    }

    int getFeatureDimension() const
    {
      return _total * _stage;
    }

    std::vector<int> getPossibleOperation()
    {
      std::vector<int> possibility;
      int score_last = _score;
      possibility.reserve(4);
      for(int i = 0; i < 4; ++i)
      {
        std::vector<int> pushed = tryPush(i);
        if(!std::equal(pushed.begin(), pushed.end(), _board.begin()))
        {
          possibility.push_back(i);
        }
      }
      _score = score_last;
      return possibility;
    }
  };
}
