//
// Created by tlou on 04.01.17.
//

#ifndef NN_TRAINER_MNIST_HPP
#define NN_TRAINER_MNIST_HPP

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include "NeuralNetwork.hpp"
#include "TypeConvertor.hpp"

namespace loco
{

  typedef unsigned char Byte;
  typedef std::vector<Byte> Feature;

  class TrainerMnist
  {
  private:
    std::vector<Feature> _train_feature;
    std::vector<Feature> _valid_feature;
    std::vector<int> _train_label;
    std::vector<int> _valid_label;
    std::vector<int> _dims;
    NeuralNetwork _network;

    bool loadFeature(const std::string &filename, std::vector<Feature> &features)
    {
      if(!boost::filesystem::is_regular_file(boost::filesystem::path(filename)))
      {
        std::cout << "feature file " << filename << " not found" << std::endl;
        return false;
      }
      std::vector<unsigned char> buffer;
      int number, length;
      buffer.resize(1024 * 1024 * 10);
      std::ifstream in;
      in.open(filename);
      in.read((char *) &buffer.front(), 16);

      if(TypeConversion::byte_to_int(buffer, 0) != 2051)
      {
        std::cout << "feature file " << filename << " is with wrong header" << std::endl;
        in.close();
        return false;
      }
      number = TypeConversion::byte_to_int(buffer, 4);
      length = TypeConversion::byte_to_int(buffer, 8) * TypeConversion::byte_to_int(buffer, 12);

      if(number * length > 10 * 1024 * 1024)
      {
        buffer.resize(number * length);
      }
      in.read((char *) &buffer.front(), number * length);
      in.close();

      features = std::vector<Feature>(number, Feature());
      for(int i = 0; i < number; ++i)
      {
        features.at(i).reserve(length);
        features.at(i).insert(features.at(i).end(),
                              buffer.begin() + i * length,
                              buffer.begin() + i * length + length);
      }
      return true;
    }

    bool loadLabel(const std::string &filename, std::vector<int> &labels)
    {
      if(!boost::filesystem::is_regular_file(boost::filesystem::path(filename)))
      {
        return false;
      }
      std::vector<unsigned char> buffer;
      int number;
      buffer.resize(1024 * 1024 * 10);
      std::ifstream in;
      in.open(filename);
      in.read((char *) &buffer.front(), 8);

      if(TypeConversion::byte_to_int(buffer, 0) != 2049)
      {
        std::cout << "feature file " << filename << " is with wrong header" << std::endl;
        in.close();
        return false;
      }
      number = TypeConversion::byte_to_int(buffer, 4);
      if(number > 10 * 1024 * 1024)
      {
        buffer.resize(number);
      }
      in.read((char *) &buffer.front(), number);
      in.close();

      labels.resize(number);
      for(int i = 0; i < number; ++i)
      {
        labels.at(i) = (int)buffer.at(i);
      }
      return true;
    }

    VectorX normalize(const Feature &feature)
    {
      float sum = 0.0f, sum_square = 0.0f;
      VectorX normalized;
      normalized.reserve(feature.size());

      for(unsigned char val : feature)
      {
        float val_f = (float)val;
        sum += val_f;
        sum_square += val_f * val_f;
      }

      float mean = sum / (float)feature.size();
      float dev = sqrt(sum_square / (float)feature.size() - mean * mean);

      for(unsigned char val : feature)
      {
        normalized.push_back(((float)val - mean) / dev);
      }

      return normalized;
    }

    float test()
    {
      assert(_valid_label.size() == _valid_feature.size());
      size_t count = 0;
      for(size_t id = 0; id < _valid_feature.size(); ++id)
      {
        if(_network.forwards(normalize(_valid_feature.at(id))) == _valid_label.at(id))
        {
          ++count;
        }
      }
      return ((float) count) / ((float) _valid_feature.size());
    }

  public:
    TrainerMnist(const std::string &filename_train_feature, const std::string &filename_train_label,
                 const std::string &filename_valid_feature, const std::string &filename_valid_label)
    {
      if(!(loadFeature(filename_train_feature, _train_feature)
           && loadFeature(filename_valid_feature, _valid_feature)
           && loadLabel(filename_train_label, _train_label)
           && loadLabel(filename_valid_label, _valid_label)))
      {
        std::cout << "data not correct loaded" << std::endl;
      }
    }

    ~TrainerMnist()
    {
    }

    void train(const size_t num_iter)
    {
      assert(_train_feature.size() == _train_label.size());
      for(size_t iter = 0; iter < num_iter; ++iter)
      {
        for(size_t id = 0; id < _train_feature.size(); ++id)
        {
          _network.forwards(normalize(_train_feature.at(id)));
          _network.backwards(_train_label.at(id));
        }
        printTest();
      }
    }

    void printTest()
    {
      std::cout << test() << std::endl;
    }

    void generateLayers(const int num_layer, const int num_neuron, const double stepsize)
    {
      _network.generateLayers(num_layer, num_neuron, stepsize);
    }

    void setDimension(const int size_in, const int size_out)
    {
      _network.setDimension(size_in, size_out);
    }

    void saveNetwork(const std::string &filename)
    {
      _network.saveNetwork(filename);
    }

    void loadNetwork(const std::string &filename)
    {
      _network = NeuralNetwork(filename);
    }
  };
}

#endif //NN_TRAINER_H
