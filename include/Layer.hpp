#ifndef NN_LAYER_HPP
#define NN_LAYER_HPP

#include <vector>
#include <iostream>
#include <cmath>
#include <memory>
#include <cblas.h>
#include "vec_math.hpp"

namespace loco
{
  class Layer
  {
  private:
    int _size_in, _size_out;
    Float _step;
    std::shared_ptr<VectorX> _weights;
    VectorX _delta;
    VectorX _result0;
    std::shared_ptr<VectorX> _result;
    VectorX _in;
    bool _is_last_layer;

  public:
    Layer(const int num_in, const int num_out, const Float step_size, const bool is_last_layer) :
        _size_in(num_in),
        _size_out(num_out),
        _step(step_size),
        _delta(_size_out * (_size_in + 1), 0.0),
        _result0(_size_out, 0.0),
        _result(new VectorX(_size_out, 0.0)),
        _in(_size_in + 1, 1.0), // last for bias
        _is_last_layer(is_last_layer),
        _weights(new VectorX())
    {
      // randomize and rescale _weights
      Float range = sqrt(6.0) / sqrt((Float) (_size_in + _size_out));
      *_weights = Math::vec_random(_size_out * (_size_in + 1));
//    cblas_dscal(_size_out * (_size_in + 1), range, &_weights->front(), 1);
      cblas_sscal(_size_out * (_size_in + 1), range, &_weights->front(), 1);
    }

    Layer(const int num_in, const int num_out, const VectorX &weights,
          const Float step_size, const bool is_last_layer) :
        _size_in(num_in),
        _size_out(num_out),
        _step(step_size),
        _delta(_size_out * (_size_in + 1), 0.0),
        _result0(_size_out, 0.0),
        _result(new VectorX(_size_out, 0.0)),
        _in(_size_in + 1, 1.0), // last for bias
        _is_last_layer(is_last_layer),
        _weights(new VectorX())
    {
      assert(_size_out * (_size_in + 1) == weights.size());
      *_weights = VectorX(_size_out * (_size_in + 1), 0.0f);
      std::copy(weights.begin(), weights.end(), _weights->begin());
    }

    ~Layer()
    {
    }

    std::shared_ptr<VectorX> forwards(const VectorX &in)
    {
      std::copy(in.begin(), in.end(), _in.begin());
//    cblas_dgemv(CblasRowMajor, CblasNoTrans, _size_out, _size_in + 1,
      cblas_sgemv(CblasRowMajor, CblasNoTrans, _size_out, _size_in + 1,
                  1.0f, &_weights->front(), _size_in + 1,
                  &_in.front(), 1, 0.0, &_result0.front(), 1);
      // TODO
//        _result = _result0;
//        sigmoid(_result0, _result);
      if(_is_last_layer)
      {
        Math::softmax(_result0, *_result);
      }
      else
      {
        Math::relu(_result0, *_result);
      }

      return _result;
    }

    std::shared_ptr<VectorX> backwards(const VectorX &diff)
    {
      VectorX rect_gradient(_size_out, 1.0);
      VectorX feedback(_size_in + 1, 0.0);
//        diff_sigmoid(_result0, rect_gradient);
      if(!_is_last_layer)
      {
        Math::diff_relu(_result0, rect_gradient);
      }
//    cblas_dger(CblasRowMajor, _size_out, _size_in + 1, -_step,
      cblas_sger(CblasRowMajor, _size_out, _size_in + 1, -_step,
                 &diff.front(), 1, &_in.front(), 1,
                 &_weights->front(), _size_in + 1);
      for(size_t id = 0; id < _size_out; ++id)
      {
        rect_gradient.at(id) *= diff.at(id);
      }
      // accumulate _delta from input and diff
//    cblas_dgemv(CblasRowMajor, CblasTrans, _size_out, _size_in + 1,
      cblas_sgemv(CblasRowMajor, CblasTrans, _size_out, _size_in + 1,
                  1.0f, &_delta.front(), _size_in + 1,
                  &rect_gradient.front(), 1, 0.0,
                  &feedback.front(), 1);

      // add _delta to _weights
//      cblas_daxpy(_size_out * (_size_in + 1), 1.0 / (double) _size_batch,
      cblas_saxpy(_size_out * (_size_in + 1), 1.0f,
                  &_weights->front(), 1, &_delta.front(), 1);
      // reset _delta
//      cblas_dscal(_size_out * (_size_in + 1), 0.0, &_delta.front(), 1);
      cblas_sscal(_size_out * (_size_in + 1), 0.0, &_delta.front(), 1);

      feedback.pop_back();
      return std::make_shared<VectorX>(feedback);
    }

    std::shared_ptr<VectorX> getOutput() const
    {
      return _result;
    }

    void setZero()
    {
      VectorX uni = Math::vec_random(_size_out);
      for(size_t id = 0; id < uni.size(); ++id)
      {
        if(uni.at(id) < (Float) 0.0)
        {
          _result0.at(id) = (Float) 0.0;
          _result->at(id) = (Float) 0.0;
        }
        else
        {
          _result0.at(id) *= (Float) 2.0;
          _result->at(id) *= (Float) 2.0;
        }
      }
    }

    int getWidth() const
    {
      return _size_in + 1;
    }

    int getHeight() const
    {
      return _size_out;
    }

    std::shared_ptr<VectorX> getWeights() const
    {
      return _weights;
    }

    void setIsLastLayer(const bool is_last)
    {
      _is_last_layer = is_last;
    }

    void setWeights(const VectorX &weights)
    {
      assert(weights.size() == _size_out * (_size_in + 1));
      *_weights = weights;
    }
  };
}

#endif
