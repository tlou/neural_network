#ifndef NN_VEC_MATH_HPP
#define NN_VEC_MATH_HPP

#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <iostream>
#include <cblas.h>

namespace loco
{
  typedef float Float;
  typedef std::vector<Float> VectorX;
  typedef std::vector<VectorX> VVectorX;

  namespace Math
  {
    Float _sigmoid(const Float in)
    {
      return 1.0 / (1.0 + exp(-in));
    }

    void leak_relu(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      vec_out = vec_in;
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        if(vec_in.at(id) < 0.0)
        {
          vec_out.at(id) = vec_in.at(id) * 0.1f;
        }
      }
    }

    void diff_leak_relu(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        if(vec_in.at(id) > 0.0)
        {
          vec_out.at(id) = 1.0;
        }
        else
        {
          vec_out.at(id) = 0.1f;
        }
      }
    }

    void relu(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        vec_out.at(id) = fmax(0.0, vec_in.at(id));
      }
    }

    void diff_relu(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        if(vec_in.at(id) > 0.0)
        {
          vec_out.at(id) = 1.0;
        }
        else
        {
          vec_out.at(id) = 0.0;
        }
      }
      //        std::cout << *vec << std::endl;
    }

    void sigmoid(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        vec_out.at(id) = _sigmoid(vec_in.at(id));
      }
    }

    void diff_sigmoid(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        Float sig = _sigmoid(vec_in.at(id));
        vec_out.at(id) = sig * (1.0 - sig);
      }
    }

    void softmax(const VectorX &vec_in, VectorX &vec_out)
    {
      assert(vec_in.size() == vec_out.size());
      Float sum = (Float) 0.0;
      for(size_t id = 0; id < vec_in.size(); ++id)
      {
        vec_out.at(id) = exp(vec_in.at(id));
        sum += vec_out.at(id);
      }
//    cblas_dscal(vec_out.size(), 1.0 / sum, &vec_out.front(), 1);
      cblas_sscal(vec_out.size(), 1.0 / sum, &vec_out.front(), 1);
    }

    VectorX vec_random(const size_t size)
    {
      VectorX vec(size, 0.0);
      for(size_t id = 0; id < size; ++id)
      {
        vec.at(id) = (Float) (rand() % 2000000 - 1000000) / 1000000.0;
      }
      return vec;
    }

    void print(const VectorX &vec)
    {
      for(size_t id = 0; id < vec.size(); ++id)
      {
        std::cout << vec.at(id) << " ";
      }
      std::cout << "\n";
    }
  }
}


#endif
