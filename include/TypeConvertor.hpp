//
// Created by tlou on 28.02.17.
//

#ifndef NN_TYPECONVERTOR_HPP
#define NN_TYPECONVERTOR_HPP

#include <vector>

namespace loco
{
  namespace TypeConversion
  {

    union Buf4Byte
    {
      char _char[4];
      unsigned char _uchar[4];
      int _int;
      float _float;
    };

    float byte_to_float(const std::vector<unsigned char> &bytes, const int offset, const bool is_inv = true)
    {
      assert(offset + 4 < bytes.size());
      Buf4Byte buf;
      if(is_inv)
      {
        for(int i = 0; i < 4; ++i)
        {
          buf._uchar[3 - i] = bytes.at(offset + i);
        }
      }
      else
      {
        for(int i = 0; i < 4; ++i)
        {
          buf._uchar[i] = bytes.at(offset + i);
        }
      }
      return buf._float;
    }

    int byte_to_int(const std::vector<unsigned char> &bytes, const int offset, const bool is_inv = true)
    {
      assert(offset + 4 < bytes.size());
      Buf4Byte buf;
      if(is_inv)
      {
        for(int i = 0; i < 4; ++i)
        {
          buf._uchar[3 - i] = bytes.at(offset + i);
        }
      }
      else
      {
        for(int i = 0; i < 4; ++i)
        {
          buf._uchar[i] = bytes.at(offset + i);
        }
      }
      return buf._int;
    }

    void write_int_to_byte(const int value, const int offset, std::vector<unsigned char> &bytes,
                           const bool is_inv = true)
    {
      assert(offset + 4 < bytes.size());
      Buf4Byte buf;
      buf._int = value;
      if(is_inv)
      {
        for(int i = 0; i < 4; ++i)
        {
          bytes.at(offset + i) = buf._uchar[3 - i];
        }
      }
      else
      {
        for(int i = 0; i < 4; ++i)
        {
          bytes.at(offset + i) = buf._uchar[i];
        }
      }
    }

    void write_float_to_byte(const float value, const int offset, std::vector<unsigned char> &bytes,
                             const bool is_inv = true)
    {
      assert(offset + 4 < bytes.size());
      Buf4Byte buf;
      buf._float = value;
      if(is_inv)
      {
        for(int i = 0; i < 4; ++i)
        {
          bytes.at(offset + i) = buf._uchar[3 - i];
        }
      }
      else
      {
        for(int i = 0; i < 4; ++i)
        {
          bytes.at(offset + i) = buf._uchar[i];
        }
      }
    }
  }
}

#endif //NN_TYPECONVERTOR_HPP
