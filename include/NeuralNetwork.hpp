#ifndef NN_NEURAL_NETWORK_HPP
#define NN_NEURAL_NETWORK_HPP

#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <memory>
#include <boost/filesystem.hpp>
#include <cblas.h>
#include "TypeConvertor.hpp"
#include "Layer.hpp"
#include "vec_math.hpp"

namespace loco
{
  class NeuralNetwork
  {
  private:
    std::vector<Layer> _layers;
    int _size_in;
    int _size_out;

    void loadNetwork(const std::string &filename)
    {
      assert(boost::filesystem::is_regular_file(boost::filesystem::path(filename)));
      // first get the size of file
      unsigned char header[] = {'n', 'n', 'b', 'y', 't', 'l', 'o', 'u'};
      std::vector<unsigned char> buffer(10 * 1024 * 1024, 0);
      int num_layer;
      std::vector<int> size_layers;
      std::vector<float> weights;
      std::ifstream in;
      std::vector<int> offsets;
      in.open(filename);

      in.read((char *) &buffer.front(), 12);
      for(int i = 0; i < 8; ++i)
      {
        assert(buffer.at(i) == header[i]);
      }
      num_layer = TypeConversion::byte_to_int(buffer, 8);

      in.read((char *) &buffer.front(), num_layer * 4);
      offsets.resize(num_layer);
      for(int i = 0; i < num_layer; ++i)
      {
        offsets.at(i) = TypeConversion::byte_to_int(buffer, 4 * i);
      }

      for(int i = 0; i < num_layer; ++i)
      {
        in.seekg(offsets.at(i), in.beg);
        in.read((char *) &buffer.front(), 8);
        int height = TypeConversion::byte_to_int(buffer, 0);
        int width = TypeConversion::byte_to_int(buffer, 4);
        int length = width * height;
        VectorX weights(length, 0.0f);
        if(length > 10 * 1024 * 1024)
        {
          buffer.resize(length);
        }
        in.read((char *) &buffer.front(), length * 4);
        for(int id = 0; id < length; ++id)
        {
          weights.at(id) = TypeConversion::byte_to_float(buffer, 4 * id);
        }
        addLayer(weights, width - 1, height);
      }

      _size_in = _layers.front().getWidth() - 1;
      _size_out = _layers.back().getHeight();

      in.close();
    }

    template<class T>
    static bool _larger_than(const std::pair<T, size_t> &obj0, const std::pair<T, size_t> &obj1)
    {
      return obj0.first > obj1.first;
    }

    template<class T>
    std::vector<int> getSortIndex(const std::vector<T> &vec)
    {
      std::vector<int> result;
      std::vector< std::pair<T, int> > vec_with_index;
      // make index
      for(size_t i = 0; i < vec.size(); ++i)
      {
        vec_with_index.push_back(std::pair<T, int>(vec.at(i), (int)i));
      }
      assert(vec_with_index.size() == vec.size());
      // sort
      std::sort(vec_with_index.begin(), vec_with_index.end(), _larger_than<T>);
      // extract result
      result.reserve(vec.size());
      for(size_t i = 0; i < vec_with_index.size(); ++i)
      {
        result.push_back(vec_with_index.at(i).second);
      }
      return result;
    }

  public:
    NeuralNetwork(const std::string &filename)
    {
      loadNetwork(filename);
    }

    NeuralNetwork()
    {
    }

    ~NeuralNetwork()
    {
    }

    void generateLayers(const int num_layer, const int num_neuron, const double stepsize)
    {
      _layers.clear();
      _layers.reserve(num_layer);
      _layers.push_back(Layer(_size_in, num_neuron, stepsize, false));
      for(int i = 1; i < num_layer - 1; ++i)
      {
        _layers.push_back(Layer(num_neuron, num_neuron, stepsize, false));
      }
      _layers.push_back(Layer(num_neuron, _size_out, stepsize, true));
    }

    void generateLayers(const std::vector<int> &num_neurons, const VectorX &stepsizes)
    {
      assert(!num_neurons.empty());
      assert((num_neurons.size() + 1) == stepsizes.size());
      _layers.clear();
      _layers.reserve(stepsizes.size());
      _layers.push_back(Layer(_size_in, num_neurons.front(), stepsizes.front(), false));
      for(int i = 1; i < num_neurons.size(); ++i)
      {
        _layers.push_back(Layer(num_neurons.at(i - 1), num_neurons.at(i), stepsizes.at(i), false));
      }
      _layers.push_back(Layer(num_neurons.back(), _size_out, stepsizes.back(), true));
    }

    int forwards(const VectorX &in, const std::vector<int> &available = std::vector<int>())
    {
      std::shared_ptr<VectorX> vec = std::make_shared<VectorX>(in);
      for(size_t id = 0; id < _layers.size(); ++id)
      {
        vec = _layers.at(id).forwards(*vec);
//        if(id < _layers.size() - 1)
//        {
//          _layers.at(id).setZero();
//        }
      }
//    return cblas_idamax((int) vec->size(), &vec->front(), 1);
//      return cblas_isamax((int) vec->size(), &vec->front(), 1);
      std::vector<int> order = getSortIndex(*vec);
      if(!available.empty())
      {
        for(int i : order)
        {
          if(std::find(available.begin(), available.end(), i) != available.end())
          {
            return i;
          }
        }
        return -1;
      }
      else
      {
        return order.front();
      }
    }

    void backwards(const int truth, const Float scale = 1.0f)
    {
      std::shared_ptr<VectorX> vec = _layers.back().getOutput();
      if(truth >= 0)
      {
        vec->at(truth) -= 1.0;
      }
      for(Float &diff : *vec)
      {
        diff *= scale;
      }
      for(size_t id = _layers.size(); id > 0; --id)
      {
        vec = _layers.at(id - 1).backwards(*vec);
      }
    }

    void saveNetwork(const std::string &filename)
    {
      namespace fs = boost::filesystem;
      fs::path path(filename);
      // create directory if not exists
      if(!fs::is_directory(path.parent_path()))
      {
        fs::create_directory(path.parent_path());
      }
      assert(fs::is_directory(path.parent_path())); // assume created, testing
      // delete file if exists
      if(fs::is_regular_file(path))
      {
        fs::remove(path);
      }

      unsigned char header[] = {'n', 'n', 'b', 'y', 't', 'l', 'o', 'u'};
      std::vector<unsigned char> bytes;
      std::ofstream out(filename.c_str(), std::ios::out | std::ios::binary);
      int offset_offset = 12, offset_layer = 12 + 4 * _layers.size();
      int count_weights = 0;
      for(size_t id = 0; id < _layers.size(); ++id)
      {
        count_weights += _layers.at(id).getHeight() * _layers.at(id).getWidth();
      }
      bytes.resize(offset_layer + 4 * count_weights + 8 * _layers.size());
      std::copy(header, header + 8, bytes.begin());
      TypeConversion::write_int_to_byte((int) _layers.size(), 8, bytes);
      for(size_t id = 0; id < _layers.size(); ++id)
      {
        const Layer &layer = _layers.at(id);
        VectorX &weights = *layer.getWeights();
        TypeConversion::write_int_to_byte(offset_layer, offset_offset, bytes);
        TypeConversion::write_int_to_byte(layer.getHeight(), offset_layer, bytes);
        TypeConversion::write_int_to_byte(layer.getWidth(), offset_layer + 4, bytes);
        for(size_t i = 0; i < weights.size(); ++i)
        {
          TypeConversion::write_float_to_byte(weights.at(i), offset_layer + i * 4 + 8, bytes);
        }
        offset_offset += 4;
        offset_layer += 8 + 4 * weights.size();
      }

      out.write((const char *) &bytes.front(), bytes.size());
      out.close();
    }

    void addLayer(const VectorX &weights, const int num_in, const int num_out)
    {
      assert((num_in + 1) * num_out == weights.size());
      if(_layers.empty())
      {
        _size_in = num_in;
      }
      else
      {
        assert(num_in == _size_out);
        _layers.back().setIsLastLayer(false);
      }
      _layers.push_back(Layer(num_in, num_out, 0.0, true));
      _layers.back().setWeights(weights);
      _size_out = num_out;
    }

    void setDimension(const int size_in, const int size_out)
    {
      _size_in = size_in;
      _size_out = size_out;
    }

    size_t getNumberLayers() const
    { return _layers.size(); }
  };
}

#endif
